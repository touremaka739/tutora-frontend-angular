import { Component } from '@angular/core';
import { HeaderComponent } from './Layouts/header/header/header.component';
import { FooterComponent } from './Layouts/footer/footer/footer.component';
import { MainComponent } from './Layouts/main/main/main.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [HeaderComponent, FooterComponent, MainComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {}
