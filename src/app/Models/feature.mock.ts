import { features } from './features';

export const FEATURES: features[] = [
  {
    name: 'Qualité Garentie',
    description:
      'Notre garantie ? Une éducation de premier ordre. Chaque enseignant sur notre plateforme est rigoureusement sélectionné et validé, assurant ainsi que tu reçois une instruction de la plus haute qualité.',
    icon: 'https://ik.imagekit.io/tnpirx7v8/parcours.svg?updatedAt=1711544649381',
  },
  {
    name: 'Flexibilité',
    description:
      'Notre plateforme te permet de trouver des enseignants adaptés à ton emploi du temps. Que tu préfères des cours en personne ou des sessions de tutorat à des moments qui te conviennent, nous offrons la flexibilité nécessaire pour que tu puisses apprendre à ton rythme et selon tes disponibilités.',

    icon: 'https://ik.imagekit.io/tnpirx7v8/apprentissage.svg?updatedAt=1711544647661/',
  },
  {
    name: 'Personnalisation',
    description:
      "Des cours conçus pour toi. Nous savons que chaque étudiant a ses propres besoins et objectifs d'apprentissage. C'est pourquoi nous proposons une mise en relation personnalisée avec des enseignants qui comprennent et s'adaptent à ta filière et à tes aspirations académiques.",
    icon: 'https://ik.imagekit.io/tnpirx7v8/mentor.svg?updatedAt=1711544649294',
  },
];
