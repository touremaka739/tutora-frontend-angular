import { Component, OnInit } from '@angular/core';
import { mockUsers } from '../../Models/user.mcok';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-user-table-component',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './user-table-component.component.html',

})

export class UserTableComponentComponent implements OnInit {
  users = mockUsers;
  ngOnInit(): void {
    console.log(this.users)
  }

}
