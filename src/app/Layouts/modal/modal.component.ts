import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
@Component({
  selector: 'app-modal',
  standalone: true,
  imports: [MatFormFieldModule],
  templateUrl: 'modal.component.html',

})
export class ModalComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<ModalComponent>) { }

  ngOnInit(): void {

  }
}
