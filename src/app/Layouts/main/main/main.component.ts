import { Component, OnInit } from '@angular/core';
import { features } from '../../../Models/features';
import { FEATURES } from '../../../Models/feature.mock';
import { CommonModule } from '@angular/common';
import { LoginModalComponent } from '../../login-modal/login-modal.component';

@Component({
  selector: 'app-main',
  standalone: true,
  imports: [CommonModule, LoginModalComponent],
  templateUrl: './main.component.html',
  styleUrl: './main.component.css',
})
export class MainComponent implements OnInit {
  features: features[] = null;
  showLoginModal: boolean = false;
  ngOnInit(): void {
    this.features = FEATURES;

    console.log(this.features);
  }

  toggleModal() {
    this.showLoginModal = !this.showLoginModal;
  }
}
