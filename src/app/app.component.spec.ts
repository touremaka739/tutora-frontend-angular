import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './Layouts/header/header/header.component';
import { MainComponent } from './Layouts/main/main/main.component';
import { FooterComponent } from './Layouts/footer/footer/footer.component';

import { RouterTestingModule } from '@angular/router/testing';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HeaderComponent,
        MainComponent,
        FooterComponent,
        RouterTestingModule,
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);

    const app = fixture.componentInstance;

    expect(app).toBeTruthy();
  });

  it('should render header, main, and footer components', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();

    expect(AppComponent).toBeTruthy();
  });
});
